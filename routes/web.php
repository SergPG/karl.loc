<?php

use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//=============  Site ===========

Route::get('/', 'Site\HomeController@index')->name('home');


//=============== Administration =========

Auth::routes();

//***************
Route::group(['prefix'=>'admin','middleware'=>'auth'],function(){
	//admin
	Route::get('/','Admin\DashboardController@index')->name('admin');


   //WelcomeSlides 
	Route::resource('welcome-slides','Admin\WelcomeSlidesController');

   //Categories 
	Route::resource('categories','Admin\CategoriesController');

   //Subcategories 
	Route::resource('subcategories','Admin\SubcategoriesController');

	 //Products
	Route::resource('products','Admin\ProductsController');


//***************************************************


});

//===================================

//***************
Route::group(['prefix'=>'admin2','middleware'=>'auth'],function(){
	//admin
	Route::get('/','Admin2\DashboardController@index')->name('admin');


   //WelcomeSlides 
	Route::resource('welcome-slides','Admin2\WelcomeSlidesController');

   //Categories 
	Route::resource('categories','Admin2\CategoriesController');

   //Subcategories 
	Route::resource('subcategories','Admin2\SubcategoriesController');

	 //Products
	Route::resource('products','Admin2\ProductsController');


//***************************************************
   
//Route::get('productajaxCRUD/{product_id?}',function($product_id){
//    $product = App\Product::find($product_id);
//    return response()->json($product);
//});

// Route::post('productajaxCRUD',function(Request $request){   
 //   $product = App\Product::create($request->input());
  //  return response()->json($product);
//});

// Route::put('productajaxCRUD/{product_id?}',function(Request $request,$product_id){
 //   $product = App\Product::find($product_id);
//   $product->name = $request->name;
 //   $product->details = $request->details;
 //   $product->save();
 //   return response()->json($product);
///});

//Route::delete('productajaxCRUD/{product_id?}',function($product_id){
   // $product = App\Product::destroy($product_id);
  //  return response()->json($product);
//  });

///==---=====----====----====----

// Route::get('productajaxCRUD/{product_id?}',function($product_id){
 //   $product = App\Product::find($product_id);
 //  return response()->json($product);
//	});

 Route::post('previewImage',function(Request $request){   
    
    $f_img = $request->file('fileImg');
    $f_n = $f_img->getClientOriginalName();
 	$url = $f_img->storeAs('public/tmp',$f_n);
 	
 	
 	  
 	  $result = [ 'url' => $url, 'nameFile' => $f_n ] ;

  	  return response()->json($result);
	});


//D:\OSPanel\domains\karl.loc\public\storage

  Route::get('testCat/{cat_id?}',function($cat_id = 0){
      $categories = App\TestCategory::all();

    return view('test_cat')->with('categories', $categories);
    }); 

});



Route::get('/home', 'HomeController@index')->name('home1');
