-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1:3306
-- Время создания: Апр 15 2018 г., 22:10
-- Версия сервера: 5.7.20
-- Версия PHP: 7.0.26

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `karl_loc`
--

-- --------------------------------------------------------

--
-- Структура таблицы `shop_categories`
--

CREATE TABLE `shop_categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `alias` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `priority` int(11) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `shop_categories`
--

INSERT INTO `shop_categories` (`id`, `name`, `alias`, `priority`, `created_at`, `updated_at`) VALUES
(1, 'Women Wear', 'Women_Wear', 1, '2018-04-14 21:00:00', '2018-04-14 21:00:00'),
(2, 'Man Wear', 'Man_Wear', 1, '2018-04-14 21:00:00', '2018-04-14 21:00:00'),
(3, 'Children', 'Children', 1, '2018-04-14 21:00:00', '2018-04-14 21:00:00'),
(4, 'Bags & Purses', 'Bags_&_Purses', 1, '2018-04-14 21:00:00', '2018-04-14 21:00:00'),
(5, 'Footwear', 'Footwear_', 1, '2018-04-14 21:00:00', '2018-04-14 21:00:00');

-- --------------------------------------------------------

--
-- Структура таблицы `shop_migrations`
--

CREATE TABLE `shop_migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `shop_migrations`
--

INSERT INTO `shop_migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(4, '2018_04_12_184635_create__table__welcome__slides', 2),
(5, '2018_04_14_201955_create__categories__table', 3),
(7, '2018_04_15_140857_create__subcategories__table', 4),
(8, '2018_04_15_173024_create__products__table', 5);

-- --------------------------------------------------------

--
-- Структура таблицы `shop_password_resets`
--

CREATE TABLE `shop_password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `shop_products`
--

CREATE TABLE `shop_products` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `size` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `quantity` int(11) NOT NULL DEFAULT '1',
  `price` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `quality` int(11) NOT NULL DEFAULT '0',
  `alias` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `manufacturer` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `subcategory_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `shop_subcategories`
--

CREATE TABLE `shop_subcategories` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `alias` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `priority` int(11) NOT NULL DEFAULT '1',
  `category_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `shop_subcategories`
--

INSERT INTO `shop_subcategories` (`id`, `name`, `alias`, `priority`, `category_id`, `created_at`, `updated_at`) VALUES
(1, 'Midi Dresses', 'Midi_Dresses', 1, 1, '2018-04-14 18:00:00', '2018-04-14 18:00:00'),
(2, 'Maxi Dresses', 'Maxi_Dresses', 1, 1, '2018-04-14 18:00:00', '2018-04-14 18:00:00'),
(3, 'Prom Dresses', 'Prom_Dresses', 1, 1, '2018-04-14 18:00:00', '2018-04-14 18:00:00'),
(4, 'Little Black Dresses', 'Little_Black_Dresses', 1, 1, '2018-04-14 18:00:00', '2018-04-14 18:00:00'),
(5, 'Man Dresses', 'Man_Dresses', 1, 2, '2018-04-14 18:00:00', '2018-04-14 18:00:00'),
(6, 'Man Black Dresses', 'Man_Black_Dresses', 1, 2, '2018-04-14 18:00:00', '2018-04-14 18:00:00'),
(7, 'Children Dresses', 'Children_Dresses', 1, 3, '2018-04-14 21:00:00', '2018-04-14 21:00:00'),
(8, 'Mini Dresses', 'Mini_Dresses', 1, 3, '2018-04-14 21:00:00', '2018-04-14 21:00:00');

-- --------------------------------------------------------

--
-- Структура таблицы `shop_users`
--

CREATE TABLE `shop_users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `shop_users`
--

INSERT INTO `shop_users` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'admin@admin.ru', '$2y$10$qas4JMji018kml5d0sVIQevG0o06oubjJ3Iv68eUxFtE7R5sracIe', NULL, '2018-04-13 07:02:59', '2018-04-13 07:02:59');

-- --------------------------------------------------------

--
-- Структура таблицы `shop_welcome_slides`
--

CREATE TABLE `shop_welcome_slides` (
  `id` int(10) UNSIGNED NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remark` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `link_text` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `link_url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `shop_welcome_slides`
--

INSERT INTO `shop_welcome_slides` (`id`, `image`, `title`, `remark`, `link_text`, `link_url`, `created_at`, `updated_at`) VALUES
(1, 'bg-1.jpg', 'Fashion Trends', '* Only today we offer free shipping', 'Shop Now', '/', '2018-04-11 21:00:00', '2018-04-11 21:00:00'),
(2, 'bg-4.jpg', 'Summer Collection', NULL, 'Check Collection', '/', '2018-04-11 21:00:00', '2018-04-11 21:00:00'),
(3, 'bg-2.jpg', 'Women Fashion', '* Only today we offer free shipping', 'Check Collection', '/', '2018-04-11 21:00:00', '2018-04-11 21:00:00');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `shop_categories`
--
ALTER TABLE `shop_categories`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `categories_name_unique` (`name`),
  ADD UNIQUE KEY `categories_alias_unique` (`alias`);

--
-- Индексы таблицы `shop_migrations`
--
ALTER TABLE `shop_migrations`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `shop_password_resets`
--
ALTER TABLE `shop_password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Индексы таблицы `shop_products`
--
ALTER TABLE `shop_products`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `products_alias_unique` (`alias`),
  ADD KEY `products_subcategory_id_foreign` (`subcategory_id`);

--
-- Индексы таблицы `shop_subcategories`
--
ALTER TABLE `shop_subcategories`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `subcategories_alias_unique` (`alias`),
  ADD KEY `subcategories_category_id_foreign` (`category_id`);

--
-- Индексы таблицы `shop_users`
--
ALTER TABLE `shop_users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Индексы таблицы `shop_welcome_slides`
--
ALTER TABLE `shop_welcome_slides`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `shop_categories`
--
ALTER TABLE `shop_categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT для таблицы `shop_migrations`
--
ALTER TABLE `shop_migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT для таблицы `shop_products`
--
ALTER TABLE `shop_products`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `shop_subcategories`
--
ALTER TABLE `shop_subcategories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT для таблицы `shop_users`
--
ALTER TABLE `shop_users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT для таблицы `shop_welcome_slides`
--
ALTER TABLE `shop_welcome_slides`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `shop_products`
--
ALTER TABLE `shop_products`
  ADD CONSTRAINT `products_subcategory_id_foreign` FOREIGN KEY (`subcategory_id`) REFERENCES `shop_subcategories` (`id`);

--
-- Ограничения внешнего ключа таблицы `shop_subcategories`
--
ALTER TABLE `shop_subcategories`
  ADD CONSTRAINT `subcategories_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `shop_categories` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
