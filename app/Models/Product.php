<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    //
	protected $table = 'products';


	protected $casts = [
    'images' => 'array',
  ];


   public function subcategory()
	{
 	 return $this->belongsTo('App\Models\Subcategory','subcategory_id','id');
	}

  

	
}
