<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Subcategory extends Model
{
    //
   protected $table = 'subcategories';

   public function category()
	{
 	 return $this->belongsTo('App\Models\Category','category_id','id');
	}


	public function products()
  {
    return $this->hasMany('App\Models\Product','id','subcategory_id');
  }

}
