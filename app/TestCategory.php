<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TestCategory extends Model
{
   
     protected $table = 'testcategory';

     public function subcategories()
	{
 	 return $this->hasMany('\App\TestCategory','parent_id','id');
	}

}
