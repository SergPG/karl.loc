<?php

namespace App\Http\Controllers\Site;

use Illuminate\Http\Request;
use App\Http\Controllers\Site\SiteController;

use App\Models\Welcome_Slide;

class HomeController extends SiteController
{
    //
   protected $model;

   public function __construct(Welcome_Slide $model)
	{

        $this->model = $model;
	}

   public function index()
   {

      $welcome_slides = $this->model->all();

     return view('site.home.index')->with([
     					'welcome_slides' => $welcome_slides,
     					]); 

   }



}
