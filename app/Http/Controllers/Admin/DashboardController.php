<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Admin\AdminController;

use App\Models\Welcome_Slide;

class DashboardController extends AdminController
{
    
   

   public function __construct(Welcome_Slide $model)
	{

        $this->model = $model;
	}

   public function index()
   {

      $welcome_slides = $this->model->all();

     return view('admin.dashboard.index')->with([
     					'welcome_slides' => $welcome_slides,
     					]); 

   }

}
