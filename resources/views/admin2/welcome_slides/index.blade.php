@extends('admin2.layouts.admin')

@section('content')
   
   
   
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Slides</h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->

            
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">  

            <div class="panel-heading">
                <span class="pull-right">
                    <button id="btnNew" class="btn btn-warning mr-3">
                    <i class="fa fa-plus-circle fa-lg " aria-hidden="true"></i> New</button>

                <div class="clearfix"></div>
            </div>
            <!-- /.panel-heading -->  
            
            
 
            <div class="panel-body">

             @if(isset($welcome_slides) && is_object($welcome_slides))


           
            <!-- ****** Welcome Slides Table ****** -->

                    <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">

                        <thead class="thead-dark">
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Image</th>
                                <th scope="col">Tetle</th>
                                <th scope="col">Remark</th>
                                <th scope="col">Link</th>
                                <th scope="col" id="Actions" >Actions</th>
                            </tr>
                        </thead>
                        <tbody>

                         @foreach( $welcome_slides as $slide )  
   
                        <tr>
                            <td scope="row" class="slide-id ">

   <button type="button" class="btn btn-info btn-circle" data-toggle="collapse" data-target="#demo-{{  $slide->id  }}"  ><i class="fa fa-lg fa-search-plus" aria-hidden="true"></i>
                            </button>

                                <span>{{  $slide->id  }}</span></td>
                            <td class="slide-image align-items-center">

                                 <button type="button" class="btn btn-default" data-container="body" data-toggle="popover" data-placement="top" data-content="{{  $slide->link_text  }}">
                                    Popover on bottom
                                </button>

                                 
                                
                                <div id="demo-{{  $slide->id  }}" class="collapse">
                                    <img src="{{asset('storage/images/bg-img').'/'.$slide->image}}" width="150" height="150"  alt="Slide"> 
                                </div>
                                

                            </td>
                            <td class="slide-title "><span> {{  $slide->title  }}</span></td>
                            <td class="slide-remark "><span>{{  $slide->remark  }}</span></td>
                            <td class="slide-link-text ">
                                <a href="{{ url($slide->link_url) }}" class="btn btn-success" role="button"  >
                                  <span>{{  $slide->link_text  }}</span>
                                </a>
                            </td>            
<td class="actions">
    <button id="btnEdit"  class="btn btn-primary btn-circle mr-1" data-toggle="modal" data-target="#SlideView"  title="Edit" value="{{  $slide->id  }}" >
        <i class="fa fa-edit fa-lg" aria-hidden="true"></i>
    </button>

    <button  id="btnDelete" class="btn btn-danger btn-circle ml-1"  title="Delete" value="{{  $slide->id  }}">
        <i class="fa fa-trash fa-lg" aria-hidden="true"></i>
    </button>
</td>
                        </tr>

                        @endforeach                      
                        
                        </tbody>
                    </table>
                        <!-- ****** END Welcome Slides Table ****** -->


            </div>
            <!-- /.panel-body --> 

        </div>
        <!-- /.panel panel-default --> 

    </div>
    <!-- /.ol-lg-12 -->
</div>
<!-- /.row -->
 @endif


    





<!-- ****** Quick View Modal Area Start ****** -->
<div class="modal fade" id="SlideView" tabindex="-1" role="dialog" aria-labelledby="SlideView" aria-hidden="true">

    <div class="modal-dialog modal-dialog-centered" role="document">
        
        <div class="modal-content">
                    
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>

                <h4 class="modal-title">Slide</h4>
            </div>
            <!-- / .modal-header -->  

<!-- Slide Form -->
<form id="frmSlide" name="frmSlide" class="form-horizontal" novalidate="" enctype="multipart/form-data">
            
            <div class="modal-body">    
                 
                <div class="form-group"> 
                    <label for="title" class="col-sm-3 control-label">Imaege</label> 
                    <div class="col-sm-9">                               
                        <img src="{{asset('storage/images/bg-img').'/'.$slide->image}}" width="100%" height="200"  alt="Slide" id="myImg"> 
                    </div> 
                </div>

                <div class="form-group">                   
                   <label for="title" class="col-sm-3 control-label"></label>
                   <div class="col-sm-9">
                     <button class="btn btn-primary" type="button" data-toggle="collapse" data-target="#imageFile" aria-expanded="false" aria-controls="imageFile">
                     Изменить</button>
                    </div> 
                </div>    
                <div class="form-group collapse" id="imageFile">                   
                   <label for="title" class="col-sm-3 control-label">File input</label>
                   <div class="col-sm-9">
                     <input type="file" class="form-control has-error" id="fileImage" name="fileImage" >
                    </div> 
                </div>          
                
                <div class="form-group">                   
                   <label for="title" class="col-sm-3 control-label">Slide title</label>
                   <div class="col-sm-9">
                     <input type="text" class="form-control has-error" id="title" name="title" placeholder="Slide title" value="">
                     </div> 
                </div>     
                <div class="form-group">                   
                   <label for="remark" class="col-sm-3 control-label">Slide remark</label>
                   <div class="col-sm-9">
                     <input type="text" class="form-control has-error" id="remark" name="remark" placeholder="Slide title" value="">
                   </div> 
                </div>   
                <div class="form-group">                   
                   <label for="remark" class="col-sm-3 control-label">Text link</label>
                   <div class="col-sm-9">
                     <input type="text" class="form-control has-error" id="link_text" name="link_text" placeholder="Slide title" value="">
                   </div> 
                </div> 
                <div class="form-group">                   
                   <label for="remark" class="col-sm-3 control-label">URL link</label>
                   <div class="col-sm-9">
                        <select class="form-control" id="link_url" name="link_url">
                            <option>1</option>
                            <option>2</option>
                            <option>3</option>
                            <option>4</option>
                            <option>5</option>
                        </select> 
                   </div> 
                </div>             
                                           

             </div>
             <!-- / .modal-body -->    

      <div class="modal-footer">

        <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>

         <button type="button" class="btn btn-primary" id="btn-save" value="add">Save changes</button>
            
        
        
      </div>  

</form>
<!-- END Slide Form -->
                
        </div>
         <!-- / .modal-content --> 

    </div>
    <!-- / .modal-dialog -->   
   
</div>
<!-- ****** Quick View Modal Area End ****** -->






 
    @push('scriptsTables')

     
        
        <script >
             // popover demo
            $("[data-toggle=popover]")
                 .popover();

        </script>
       
<script >

(function($) {
  // Start of use strict

   $(document).on('click','#btnEdit',function(e){
        
    var slide_id = $(this).val();
    console.log('Slide_id:', slide_id);

     $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
            }
        })
        e.preventDefault(); 


// console.log('formData:', formData);

    
    });
   // .change(function () {  webkitRelativePath



     $('#fileImage').change(function () {
 var control = document.getElementById("fileImage");
 var formData = new FormData();       

       //var filePath = control.files;
  
  // console.log('PathFile:', filePath );
   //var formData = new FormData();

       var filePath;
       jQuery.each(control.files, function(i, file) {
             
             formData.append("fileImg", file);
         });

 
formData.append("username", "Groucho");
formData.append("accountnum", 123456); 
// число 123456 немедленно преобразуется в строку "123456"

     ///===========  http://karl.loc/admin2/welcome-slides
        $.ajax({
            type: "POST",
            url: "http://karl.loc/admin2/previewImage",
             cache: false,
            contentType: false,
            processData: false,
            data: formData,

            dataType: 'json',
            success: function (data) {
                console.log(data);


    var my_img = '<img src="http://karl.loc/storage/tmp/'+ data.nameFile +'" width="100%" height="200"  alt="Slide" id="myImg">  ';
             $("#myImg").replaceWith( my_img );     
             
            },
            error: function (data) {
                console.log('Error:', data);
            }
        });


// D:\OSPanel\domains\karl.loc\public\storage\tmp
   });



  
///=============================

    $(document).on('click','#btn-save',function(){
        
    

  //  var control = document.getElementById("fileImage");
   
   //var formData = new FormData();

    //   jQuery.each(control.files, function(i, file) {
             

     //        console.log('FormData:', file.name);
      //      });
 
       
     
     });
///=============================


})(jQuery); // End of use strict
 </script>

    @endpush


 <meta name="_token" content="{!! csrf_token() !!}" />

    
@endsection

		