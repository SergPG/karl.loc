<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="description" content="">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- The above 4 meta tags *must* come first in the head; any other head content must come *after* these tags -->

    <!-- Title  -->
    <title>Karl - Fashion Ecommerce Template | Home</title>

    <!-- Favicon  -->
    <link rel="icon" href="{{asset('storage/images/core-img/favicon.ico')}}">

    <!-- Core Style CSS -->
    <link rel="stylesheet" href="{{asset('theme/admin/css/core-style.css')}}">
    <link rel="stylesheet" href="{{asset('theme/admin/admin.css')}}">

    <!-- Responsive CSS -->
    <link href="{{asset('theme/admin/css/responsive.css')}}" rel="stylesheet">

</head>

<body>

<!-- .catagories-side-menu -->
    @section('catagories_side_menu')
        @include('admin.layouts.design.catagories_side_menu') 
    @show   
<!-- END .catagories-side-menu -->
   

<div id="wrapper">

    <!-- ****** Header Area Start ****** -->
        <header class="header_area">
   
          <!-- Top Header Area Start -->
            @section('top_header_area')
               @include('admin.layouts.design.top_header_area') 
            @show 
          <!-- Top Header Area End -->

          <!-- Main Header Area Start -->
            @section('main_header_area')
               @include('admin.layouts.design.main_header_area') 
            @show 
          <!-- Main Header Area End -->
           
        </header>
    <!-- ****** Header Area End ****** -->

   

<!-- Main Content -->
    @yield('content')
<!-- END Main Content -->

   

    <!-- ****** Footer Area Start ****** -->
        @section('footer_area')
            @include('admin.layouts.design.footer_area') 
        @show 

    <!-- ****** Footer Area End ****** -->

  </div>
 <!-- /.wrapper end -->

    <!-- jQuery (Necessary for All JavaScript Plugins) -->
    <script src="{{asset('theme/admin/js/jquery/jquery-2.2.4.min.js')}}"></script>
    <!-- Popper js -->
    <script src="{{asset('theme/admin/js/popper.min.js')}}"></script>
    <!-- Bootstrap js -->
    <script src="{{asset('theme/admin/js/bootstrap.min.js')}}"></script>
    <!-- Plugins js -->
    <script src="{{asset('theme/admin/js/plugins.js')}}"></script>
    <!-- Active js -->
    <script src="{{asset('theme/admin/js/active.js')}}"></script>

</body>

</html>