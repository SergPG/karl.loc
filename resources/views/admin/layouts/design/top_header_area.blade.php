        <div class="top_header_area">
                <div class="container h-100">
                    <div class="row h-100 align-items-center justify-content-end">

                        <div class="col-12 col-lg-7">
                            <div class="top_single_area d-flex align-items-center">
                               
                                <!-- Logo Area -->
                                <div class="top_logo">
                                    <a href="#"><img src="{{asset('storage/images/core-img/logo.png')}}" alt=""></a>
                                </div>
                                <!-- END Logo Area -->

                                <!-- Cart & Menu Area -->
                                <div class="header-cart-menu d-flex align-items-center ml-auto">
                                    
<!-- Cart Area -->
    @include('site.layouts.design.top_cart_area')   
<!-- END Cart Area -->                                   

                                    <div class="header-right-side-menu ml-15">
                                        <a href="#" id="sideMenuBtn"><i class="ti-menu" aria-hidden="true"></i></a>
                                    </div>
                                </div>
                                <!-- END Cart & Menu Area -->
                            
                            </div>
                        </div>

                    </div>
                </div>
            </div>