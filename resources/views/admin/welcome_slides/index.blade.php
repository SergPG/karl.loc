@extends('admin.layouts.admin')

@section('content')
   
   
   
<div class="container  ">
    <div class="row">
        <div class="col-12 offer-content-area">

            
            <h2 class="text-center">Slides</h2>   
            <p class="text-right">
                <a href="#" class="btn btn-outline-warning mr-3">
                <i class="fa fa-pencil fa-lg mr-1 " aria-hidden="true"></i>New</a>
            </p>
 


 @if(isset($welcome_slides) && is_object($welcome_slides))


           
<!-- ****** Welcome Slides Table ****** -->
            <div class="table">
                <table class="table table-hover text-center">
                     <thead class="thead-dark">
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Image</th>
                            <th scope="col">Tetle</th>
                            <th scope="col">Remark</th>
                            <th scope="col">Link</th>
                            <th scope="col">Actions</th>
                        </tr>
                    </thead>
                    <tbody>

    @foreach( $welcome_slides as $slide )  
    

    <tr>
        <td scope="row" class="slide-id "><span>{{  $slide->id  }}</span></td>
        <td class="slide-image align-items-center">
            <img src="{{asset('storage/images/bg-img').'/'.$slide->image}}" width="150" height="150"  alt="Slide"> 
        </td>
        <td class="slide-title "><span> {{  $slide->title  }}</span></td>
        <td class="slide-remark "><span>{{  $slide->remark  }}</span></td>
        <td class="slide-link-text ">
            <a href="{{ url($slide->link_url) }}" class="btn btn-outline-dark" role="button"  >
             <span>{{  $slide->link_text  }}</span>
            </a>
            

        </td>
                            
        <td class="actions">
          <a href="checkout.html" class="btn btn-outline-primary mr-1" data-toggle="modal" data-target="#SlideView" role="button" title="Edit" >
             <i class="fa fa-edit fa-lg" aria-hidden="true"></i>
            </a>

            <a href="checkout.html" class="btn btn-outline-danger ml-1" role="button" title="Delete">
             <i class="fa fa-trash fa-lg" aria-hidden="true"></i>
            </a>
        </td>
    </tr>

    @endforeach                      
                        
                    </tbody>
                </table>
  <!-- ****** END Welcome Slides Table ****** -->

            </div>
        </div>
    </div>
</div>

 @endif


    



<!-- ****** Quick View Modal Area Start ****** -->
<div class="modal fade" id="SlideView" tabindex="-1" role="dialog" aria-labelledby="SlideView" aria-hidden="true">

    <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
        <div class="modal-content">
                    
                <div class="modal-header">
                   <h4 class="modal-title" id="myModalLabel">Slide</h4>

                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div class="modal-body">

                    <div class="quickview_body">
                        <div class="container">
                            <div class="row">  

                                    <div class="col-12 col-lg-5">        
                                        <div class="quickview_pro_img">
                                          <img src="{{asset('storage/images/bg-img').'/'.$slide->image}}" alt="Slide"> 
                                        </div>
                                    </div>
                            
                                    <div class="col-12 col-lg-7">

                                     <!-- Slide Form -->

                                        <form class="cart" method="post">

                                        <div class="quickview_pro_des">
                                            <h4 class="title"> {{  $slide->title  }} </h4>
                                            <h6 class="price">{{  $slide->remark  }}</span></h6>
                                        </div>

                                           <button type="submit" name="addtocart" value="5" class="cart-submit">Add to cart</button>           
                                        </form>
                                    </div>
                 
                            </div>
                        </div>
                    </div>

                </div>
        </div>
    </div>
</div>
<!-- ****** Quick View Modal Area End ****** -->








    
@endsection

		