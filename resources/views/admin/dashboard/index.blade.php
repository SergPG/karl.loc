@extends('admin.layouts.admin')

@section('content')

    <!-- ****** Top Discount Area Start ****** -->
        @section('top_discount_area')
            @include('admin.layouts.design.top_discount_area') 
        @show   
    <!-- ****** Top Discount Area End ****** -->


    <!-- ****** Welcome Slides Area Start ****** -->
        @section('welcome_slides')
            @include('admin.layouts.design.welcome_slides') 
        @show 
    <!-- ****** Welcome Slides Area End ****** -->
	
	<!-- ****** Top Catagory Area Start ****** -->

        @section('top_catagory_area')
            @include('admin.layouts.design.top_catagory_area') 
        @show  
        
    <!-- ****** Top Catagory Area End ****** -->

    <!-- ****** Quick View Modal Area Start ****** -->

        @section('quick_view_modal')
            @include('admin.layouts.design.quick_view_modal') 
        @show  
        
    <!-- ****** Quick View Modal Area End ****** -->

    <!-- ****** New Arrivals Area Start ****** -->
        @section('new_arrivals_area')
            @include('admin.layouts.design.new_arrivals_area') 
        @show  
  
    <!-- ****** New Arrivals Area End ****** -->

    <!-- ****** Offer Area Start ****** -->
        @section('offer_area')
            @include('admin.layouts.design.offer_area') 
        @show  
       
    <!-- ****** Offer Area End ****** -->

    <!-- ****** Popular Brands Area Start ****** -->
        @section('popular_brands_area')
            @include('admin.layouts.design.popular_brands_area') 
        @show 
      
    <!-- ****** Popular Brands Area End ****** -->
	
	
    
@endsection

		