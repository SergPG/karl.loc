	<div class="cart">
        <a href="#" id="header-cart-btn" target="_blank"><span class="cart_quantity">2</span> <i class="ti-bag"></i> Your Bag $20</a>
                                        
    <!-- Cart List Area Start -->
        <ul class="cart-list">
            <li>
                <a href="#" class="image"><img src="{{asset('storage/images/product-img/product-10.jpg')}}" class="cart-thumb" alt=""></a>
                <div class="cart-item-desc">
                    <h6><a href="#">Women's Fashion</a></h6>
                    <p>1x - <span class="price">$10</span></p>
                </div>
                <span class="dropdown-product-remove"><i class="icon-cross"></i></span>
            </li>
            <li>
                <a href="#" class="image"><img src="{{asset('storage/images/product-img/product-11.jpg')}}" class="cart-thumb" alt=""></a>
                <div class="cart-item-desc">
                    <h6><a href="#">Women's Fashion</a></h6>
                        <p>1x - <span class="price">$10</span></p>
                </div>
                <span class="dropdown-product-remove"><i class="icon-cross"></i></span>
            </li>
            <li class="total">
                <span class="pull-right">Total: $20.00</span>
                    <a href="cart.html" class="btn btn-sm btn-cart">Cart</a>
                    <a href="checkout-1.html" class="btn btn-sm btn-checkout">Checkout</a>
            </li>
        </ul>
    </div>